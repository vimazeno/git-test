https://www.atlassian.com/git/tutorials/git-subtree

## Adding the sub-project as a remote

Adding the subtree as a remote allows us to refer to it in shorter form:

```
git remote add -f git-subtree git@gitlab.isima.fr:vimazeno/git-subtree.git
```

Add the subtree by referring to the remote in short form:

```
git subtree add --prefix features/git-subtree git-subtree master --squash
```

Update git subtree anytime:

```
git fetch git-subtree master
git subtree pull --prefix features/git-subtree git-subtree master --squash
```

## Contributing back upstream 

record a merge so that we can track the entire tree history of the plug-in up to this point:

```
git merge -s ours --no-commit git-subtree/main
```

Read the content of the latest tree-object into the plugin repository into our working directory ready to be committed:

```
git read-tree --prefix=features/git-subtree -u git-subtree/main
```

commit on subtree

```
git ci -m"[subtree] adding new feature"
```

update git-subtree repo git pull -s subtree tpope-vim-surround main